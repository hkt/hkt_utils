/* IO */

// Read numbers
//
int number;
while (std::cin >> number) {

}

// Read lines with local line endings
//
std::string line;
std::getline(std::cin, line) {

}

// Read lines regardles of line ending LF, CR, CRLF
// (replacement of std::getline)
std::istream& unigetline(std::istream& is, std::string& line)
{
    line.clear();

    // The characters in the stream are read one-by-one using a std::streambuf.
    // That is faster than reading them one-by-one using the std::istream.
    // Code that uses streambuf this way must be guarded by a sentry object.
    // The sentry object performs various tasks,
    // such as thread synchronization and updating the stream state.

    std::istream::sentry se(is, true);
    std::streambuf* sb = is.rdbuf();

    for(;;) {
        int c = sb->sbumpc();
        switch (c) {
        case '\n':
            return is;
        case '\r':
            if(sb->sgetc() == '\n')
                sb->sbumpc();
            return is;
        case std::streambuf::traits_type::eof():
            is.setstate(std::ios::eofbit);       //
            if(line.empty())                        // <== change here
                is.setstate(std::ios::failbit);  //
            return is;
        default:
            line += (char)c;
        }
    }
}



// Wczytywanie pliku do własnego bufora char*
//
std::pair<const char*, size_t> read_file(const char* filename)
{
    if (!filename)
        return std::make_pair(nullptr, 0);

    std::ifstream fin(filename, std::ios::binary);

    if (!fin.good())
        return std::make_pair(nullptr, 0);

    std::filebuf* pbuf = fin.rdbuf();
    auto size = pbuf->pubseekoff(0, std::ios::end, std::ios::in);
    if (size == 0)
        return std::make_pair(nullptr, 0);

    if (size > std::numeric_limits<GLint>::max())
        throw std::runtime_error("shader is too large");

    char* text = new char[size]; // we pass ownership to the caller
    pbuf->pubseekpos(0, std::ios::in);
    pbuf->sgetn(text, size);
    fin.close();

    return std::make_pair(text, size);
}




// Universal print
// Usage: print("print: ", "a", 3.14f);
//
template <typename ...Args>
void print(const Args& ...args) {
  (std::cout << ... << args);
}

// Unniversal print with line break
// Usage: println("print: ", "a", 3.14f);
//
template <typename ...Args>
void println(const Args& ...args) {
  (std::cout << ... << args) << "\n";
}


std::vector<std::vector<std::string>> read_csv(const char* path)
{
  std::ifstream input{path};

  std::vector<std::vector<std::string>> rows;
  if (!input.is_open()) {
    std::cerr << "Couldn't read file: " << filename << "\n";
    return rows; 
  }

  for (std::string line; std::getline(input, line);) {
    std::istringstream ss(std::move(line));
    std::vector<std::string> row;
    if (!rows.empty()) {
       // We expect each row to be as big as the first row
      row.reserve(rows.front().size());
    }
    // std::getline can split on other characters, here we use ','
    for (std::string value; std::getline(ss, value, ',');) {
      row.push_back(std::move(value));
    }
    rows.push_back(std::move(row));
  }
  
  return rows;
}
