/* Math */
#include <cstdint>

// Signum function
template <class T>
inline int sign (const T& x) {
    return x > T{0} ? 1 : (x < T{0} ? -1 : 0);
}


// Powers of integers
// Computes b**e (mod UINT32_MAX)
uint32_t
ipow(uint32_t b, uint32_t e)
{
  uint32_t ret;
  for (ret = 1; e; e >>= 1) {
    if (e & 1) {
      ret *= b;
    }
    b *= b;
  }
  return ret;
}

// Power mod m
// Computes b**e (mod m)
uint32_t
ipowm(uint32_t b, uint32_t e, uint32_t m)
{
  uint32_t ret;
  b %= m;
  for (ret = m > 1; e; e >>= 1) {
    if (e & 1) {
      ret = (uint64_t)ret * b % m;
    }
    b = (uint64_t)b * b % m;
  }
  return ret;
}





// Remap value from [ilo, ihi] to [olo, ohi]
//
float remap(float value, float from_min, float from_max, float to_min, float to_max)
{
    return to_min + (to_max - to_min)*((value - from_min)/(from_max - from_min));
}


// Midpoint/Average without overflow
//
// smallest value no smaller than (x + y)/2
//unsigned int midpoint(unsigned int x,unsigned int y) { 
//  return (x|y) - ((x^y)>>1); 
//}

// largest value no larger thant (x + y)/2
//unsigned int midpoint(unsigned int x,unsigned int y) { 
//  return ((x^y)>>1) + (x&y); 
//}




// Number of digits of integer
//
inline int digits10(long long val) {
    uint32_t result = 1;
    for (;;) {
        if (val < 10) return result;
        if (val < 100) return result + 1;
        if (val < 1000) return result + 2;
        if (val < 10000) return result + 3;
        // Skip ahead by 4 orders of magnitude
        val/=1000U;
        result +=4;
    }
}

// Is power of 2
//
bool is_power_of_2(int i) {
return i > 0 && (i & (i - 1)) == 0;
}

// Is power of 2 with check for incidental cast
//
//template <typename T>
//bool is_power_of_2(T i) {
//    static_assert(std::is_integral_v<T>);
//    return i > 0 && (i & (i - 1)) == 0;
//}

// Which power of 2 is the number (log2)
// 
constexpr int log2(unsigned int word) {
    return word ? (1 + log2(word>>1)) : 0;
}


