# C++ Functions #

 - [Math](math_functions.cpp) - mathematical functions
 - [Graphics](graphics_functions.cpp) - geometry
 - [Hashing](hash_functions.cpp) - hashing functions
 - [System](system_functions.cpp) - system interaction
 - [IO](io_functions.cpp) - io functions and text files
 - [Format](format_functions.cpp) - formatting
 - [Strings](string_functions.cpp) - string operations
 - [Meta](meta_functions.cpp) - metaprogramming
 - [Endianness](endianness_functions.cpp) - endianess
 - [Net](net_functions.cpp) - networking
 - [Embedded](embedded_functions.cpp) - useful embedded functions
 - [Physics](physics_functions.cpp) - physics functions
