/* Net */

// Udp send
//
class Socket {
public:
  Socket() {
    sockfd = socket(PF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0)
      throw std::runtime_error("Nie można utworzyć socketa");
  }
  ~Socket() { close(sockfd); }
  void bind(long port) {
    struct sockaddr_in sender_addr;
    memset(&sender_addr, 0, sizeof(sender_addr));

    // Filling server information
    sender_addr.sin_family = AF_INET; // IPv4
    sender_addr.sin_addr.s_addr = INADDR_ANY;
    sender_addr.sin_port = htons(port);

    // Bind the socket with the server address
    int result = ::bind(sockfd, (const struct sockaddr *)&sender_addr,
                        sizeof(sender_addr));
    if (result < 0)
      throw std::runtime_error("Nie można wywołać bind()");
  }
  void sendto(std::string addr, long port, std::string message) {
    // Build destination address
    struct sockaddr_in dest;
    dest.sin_family = AF_INET;
    inet_aton(addr.c_str(), &dest.sin_addr);
    dest.sin_port = htons(port);
    ::sendto(sockfd, message.c_str(), message.length(), MSG_CONFIRM,
             (const struct sockaddr *)&dest, sizeof(dest));
  }

private:
  int sockfd;
};
