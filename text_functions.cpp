/* Text */
#include <string>
#include <string_view>
#include <cstdlib>
#include <cstring>
#include <vector>
#include <algorithm>
#include <locale>
// String qualification
//

bool isdecimal(std::string_view s)
{
    return !s.empty() > 0 && std::all_of(s.begin(), s.end(), ::isdigit);
}


// C functions for padding
//
char* ljust(char* d, const char* s, size_t width, char fillchar)
{
    size_t len = strlen(s);
    strncpy(d, s, width);
    size_t fillwidth = width - len;
    memset(d + len, fillchar, fillwidth);
    d[width] = '\0';
    return d;
}

char* rjust(char* d, const char* s, size_t width, char fillchar)
{
    size_t len = strlen(s);
    size_t fillwidth = width - len;
    memset(d, fillchar, fillwidth);
    strncpy(d + fillwidth, s, width - fillwidth);
    d[width] = '\0';
    return d;
}

// Trimming 
//
std::string& ltrim(std::string &s)
{
    auto it = std::find_if(s.begin(), s.end(),
                [](char c) {
                    return !std::isspace<char>(c, std::locale::classic());
                });
    s.erase(s.begin(), it);
    return s;
}
 
std::string& rtrim(std::string &s)
{
    auto it = std::find_if(s.rbegin(), s.rend(),
                [](char c) {
                    return !std::isspace<char>(c, std::locale::classic());
                });
    s.erase(it.base(), s.end());
    return s;
}
 
std::string& trim(std::string &s) {
    return ltrim(rtrim(s));
}

// String split
//
std::vector<std::string_view>
split(std::string_view str, std::string_view delims = " ")
{
    std::vector<std::string_view> output;
    auto first = std::cbegin(str);

    while (first != std::cend(str))
    {
        const auto second = std::find_first_of(first, std::cend(str),
                  std::cbegin(delims), std::cend(delims));

        if (first != second)
            output.emplace_back(first, second-first);

        if (second == std::cend(str))
            break;

        first = second + 1;
    }

    return output;
}

// Hex string to integer
int xtoi(std::string_view s)
{
    auto xchrtoi = [](char c) {
        return c >= 'a' && c <= 'f' ?
            c - 'a' + 0xa :
            ((c >= 'A' && c <= 'F') ? c - 'A' + 0xa : c - '0');
    };

    int sum = 0;

    for (const auto& c : s) {
        sum *= 16;
        sum += xchrtoi(c);
    }

    return sum;
}

std::string strip_parenthesized(std::string_view s)
{
    std::string out;
    int level = 0;
    for (auto pos = 0ull; pos < s.size(); ++pos) {
        switch(s[pos]) {
            case '(':
                ++level;
                break;
            case ')':
                level = (level > 0) ? level - 1: 0;
                break;
            default:
                if (level == 0) 
                    out += s[pos];
        }
    }
    return out;
}




