/* Colour */

#include <cmath>
#include <cstdlib>
#include <array>

// Palette generating functions
//


std::array<unsigned char, 3> cga_palette(unsigned int n) {
    std::array<unsigned char, 3> rgb;

    unsigned char base = (n & (1u << 3)) ? 0x55 : 0x00;
    for (int i = 0; i < 3; ++i) {
       rgb[2 - i] = (n & (1 << i)) ? base + 0xAA : base;
    }
    // correct brown not to become dark yellow
    rgb[1] -= ((n^0x6) == 0) ? 0x55 : 0;
    return rgb;
}



// RGB -> HSV
//

typedef struct {
    union {
        float r;
        float h;
    };
    union {
        float g;
        float s;
    };
    union {
        float b;
        float v;
    };
} colour_st;

const float EPSILON = 0.02f;

void convert(colour_st* d, colour_st* s)
{
   float tmp;
   tmp = std::min(std::min(s->r,s->g), s->b);
   d->v = std::max(std::max(s->r,s->g),s->b);
   if (fabs(tmp - d->v) < EPSILON)
       d->h = 0;
   else {
       if (fabs(s->r - d->v) < EPSILON)
           d->h = 0 + (s->g - s->b)*60/(d->v - tmp);

       if (fabs(s->g - d->v) < EPSILON)
           d->h = 120 + (s->b - s->r)*60/(d->v - tmp);

       if (fabs(s->b - d->v) < EPSILON)
           d->h = 240 + (s->r - s->g)*60/(d->v - tmp);
   }
   if (d->h < 0)
       d->h += 360;

   if (fabs(d->v) < EPSILON) {
       d->s = 0;
   }
   else
       d->s = (d->v - tmp)*100/(d->v);

   d->v = (100*(d->v))/255;
}

