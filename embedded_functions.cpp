/* Embedded */

// BCD to 7-segment display
//
int BCDto7seg(int l)
{
    bool x,y,z,w;
    x = l & (1<<3);
    y = l & (1<<2);
    z = l & (1<<1);
    w = l & (1<<0);

    int a,b,c,d,e,f,g;
    a = (!y && !w) || ( y && z ) || ( y && !w) ? 1 : 0;
    b = (!y && !w) || (!z && w) ? 1 : 0;
    c = (!y && !w) || (!y && w) || (!z && w) ? 1 : 0;
    d = (!y && w) || (!z && w) || (y && !w) ? 1 : 0;
    e = 1;
    f = (!y && !w) || (!y && w) || (y && z) || (y && !w) ? 1 : 0;
    g = (!y && !w) || (!y && w) || (!z && w) || (y && z) ? 1 : 0;

    return (g << 6)|(f << 5)|(e << 4)|(d << 3)|(c << 2)|(b << 1)|a;
}

