 /* Physics */

double pressure_at_sealevel(double atm_p, int altitude)
{
/* equation for computing the pressure is valid for altitude
 * up to 10km:
 *
 *  p0 = p / pow((1 + altitude * (L/T0)) , -(gn*M)/(R*L) );
 *
 *  p - pressure measured [ hPa ]
 *  L - temperature lapse -0.0065 [ K / m ]
 *  T0 - standard temperature at sea level 288.15 K (15 °C)
 *  gn - standard gravity 9.80665 [ m / s*s ]
 *  M - average molar mass of dry air 0.0289644 [ kg / mol ]
 *  R - gas constant 8.314462611815324 [ (N*m) / (mol*K) ]
*/

const double ALT_T_COEFF = -0.00002255769564462953; // L/T0
const double EXPONENT = 5.25578596313068172618; // -(gn*M)/(R*L) 
   
    return atm_p/pow((1 + altitude*ALT_T_COEFF), EXPONENT);
}


/* Conversion from °C to °F
*/

double to_fahrenheit(double celsius)
{
    return 1.8*celsius + 32;
}
