/* System */
#include <string_view> // getenvvar
#include <cstdio>      // print_envs

// Dump env variables to stdout
//
void print_envs()
{
    extern char** environ;
    for (char** envp = environ; *envp; envp++) {
        puts(*envp);
    }
}

// Get value of env variable
//
std::string_view getenvvar(const std::string_view key)
{
    extern char** environ; // defined by POSIX
    auto len = std::size(key);
    for (char** envp = environ; *envp; envp++) 
        if (std::equal(key.begin(), key.end(), *envp) && (*envp)[len] == '=') {
            return std::string_view(*envp + len + 1);
    }
    return "";
}


// Load dynamic library (Linux)
//
#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>

int dlopen_example(int argc, char **argv) {
  void *handle;
  double (*cosine)(double);
  char *error;

  handle = dlopen("libm.so.6", RTLD_LAZY);
  if (!handle) {
    fputs(dlerror(), stderr);
    exit(1);
  }
  // load cosine function
  cosine = (double (*)(double)) dlsym(handle, "cos");
  if ((error = dlerror()) != NULL) {
    fputs(error, stderr);
    exit(1);
  }

  printf("%f\n", (*cosine)(2.0));
  return dlclose(handle);
}

