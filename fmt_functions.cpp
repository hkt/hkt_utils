 /* format */

// Oneliner to convert single digit hex number to its value {0, 16}
//
auto xtoi =[](char c) {
    return '0' <= c && c <= '9' ? c - '0' :
        (('A' <= c && c <= 'F') ? c - 'A' + 0xa : c - 'a' + 0xa);
}


// Hex str to long
//
inline long hextol(const char* s) {
    long val = 0;
    // 0x prefix check
    // if (*s == '0' && *(s + 1) == 'x')
    //      s+=2;
    while (*s) {
        val<<=4; // first loop shifts 0 by 4
        if (*s >= '0' && *s <='9') {
            val += *s - '0';
        }
        else
            if (*s >= 'A' && *s <= 'F') {
                val += *s - 'A' + 0xA;
            }
            else
                if (*s >= 'a' && *s <= 'f') {
                    val += *s - 'a' + 0xA;
                }
        ++s;
    }
    return val;
}


// Convert value to human readable with SI suffix
//
template <typename T>
constexpr std::pair<float, char> human_readable_si(T value)
{
    if (-1000 < value && value < 1000)
        return std::make_pair(value, ' ');
    const char* suf_p = "kMGTPE";
    for (; value <= -999950 || value >= 999950; ++suf_p)
        value /= 1000;
    float fvalue{value};
    fvalue = round(fvalue/100);
    fvalue /=10;
    return std::make_pair(fvalue, *suf_p);
}

// Print hex value
//
void print_hex(const char* p)
{
    while(*p)
    {
        char val = ((*p)>>4) & 0xF;
        putchar( ( val <= 9 ) ?  val + '0': val + 'A' - 0x0A);
        val = (*p) & 0xF;
        putchar( ( val <= 9 ) ?  val + '0': val + 'A' - 0x0A);
        ++p;
    }
    putchar('\n');
}

