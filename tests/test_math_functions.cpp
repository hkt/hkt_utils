
#include "../math_functions.cpp"
#include <iostream>

template <typename Param, typename Result>
bool run_sign(Param p, Result result, bool verbose = false)
{
    bool ok = (sign(p) == result); 
    if (ok) {
        if (verbose)
            std::cout << "[ OK ] (" << p << "): " << result << '\n';
        return true;
    }
    else {
        if (verbose)
            std::cout << "[FAIL] (" << p << "): " << sign(p) << '\n';
        return false;
    }
    return ok; 
}


int main(int argc, char* argv[])
{
    bool is_verbose = true;
    bool all_ok = true;
    all_ok &= run_sign( 1, 1, is_verbose);
    all_ok &= run_sign( -1, -1, is_verbose);
    all_ok &= run_sign( 0, 0,  is_verbose);
    all_ok &= run_sign( 10098, 1, is_verbose);
    all_ok &= run_sign( -1239807, -1, is_verbose);
    all_ok &= run_sign( 999999, 1, is_verbose);
    return (all_ok) ? 0 : -1; 
}

