#include "../text_functions.cpp"

#include <string>
#include <iostream>
#include <iomanip>

bool test_strip_parenthesized()
{
    std::vector<std::pair<std::string, std::string>> test_set = {
        { "((asd./.45))w", "w" },
        { "(Alus) (aptam)", " "}
    };

    bool all_ok = true;
    for (const auto& test : test_set) {
        bool success = strip_parenthesized(test.first) == test.second;
        if (!success) {
            std::cerr << "[FAIL] " << std::quoted(test.first) << "-> " << std::quoted(strip_parenthesized(test.first));
            std::cerr << '\n';
            all_ok = false;
        }
    }
        
    return all_ok;
}


int main(int argc, char *argv[])
{
    bool all_ok = true;
    all_ok = all_ok && test_strip_parenthesized();
    return all_ok ? 0 : -1;
}
