#include <iostream>
#include <iomanip>
#include "../colour_functions.cpp"


int g_verbosity = 0;





int main(int argc, char* argv[])
{
    for (int argi = 1; argi < argc; ++argi)
        if (argv[argi][0] == '-')
            for (auto argp = &argv[argi][1]; *argp == 'v'; ++argp)
                ++g_verbosity;

    const std::array<unsigned char, 3> CGA_RGB[] = {
        {0x00, 0x00, 0x00}, // 0
        {0x00, 0x00, 0xaa}, // 1
        {0x00, 0xaa, 0x00}, // 2
        {0x00, 0xaa, 0xaa}, // 3
        {0xaa, 0x00, 0x00}, // 4
        {0xaa, 0x00, 0xaa}, // 5
        {0xaa, 0x55, 0x00}, // 6
        {0xaa, 0xaa, 0xaa}, // 7
        {0x55, 0x55, 0x55}, // 8
        {0x55, 0x55, 0xff}, // 9
        {0x55, 0xff, 0x55}, // 10
        {0x55, 0xff, 0xff}, // 11
        {0xff, 0x55, 0x55}, // 12
        {0xff, 0x55, 0xff}, // 13
        {0xff, 0xff, 0x55}, // 14
        {0xff, 0xff, 0xff}  // 15
    };

    bool passed = true;
    for (auto n = 0u; n < 16; ++n) {
       auto [R, G, B] = CGA_RGB[n];
       auto [r, g, b] = cga_palette(n); 

       if (g_verbosity) 
           std::cout << n << std::setw(3) << ". " << +R << ',' << +G << ',' << +B << "\n";

       if (r == R && g == G && b == B) 
           passed = passed && true; 
       else {
           std::cout << "colour mismatch at n: " << n;
           std::cout << "rgb: " << +r << ',' << +g << ',' << +b << " != ";
           std::cout <<            +R << ',' << +G << ',' << +B << "\n";
           passed = false;
       }
    }

    return (passed) ? 0 : -1;
}
