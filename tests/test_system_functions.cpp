#include "../system_functions.cpp"
#include <string_view>
#include <iostream>

bool test_getenvvar(bool verbose)
{
    auto home = getenvvar("HOME");
    if (verbose)
        std::clog << "HOME:" << home << '\n';
    auto naoi8s = getenvvar("naoi8s");
    if (verbose)
        std::clog << "naoi8s:" << naoi8s << '\n';
    if (home.length() > 0 && naoi8s.length() == 0)
        return true;
    return false;
}


int main(int argc, char* argv[])
{
    if (argc == 1 + 1)
        if (std::string_view(argv[1]) == "-v")
    return test_getenvvar(true) ? 0 : -1;
}

