/* Endianness */

// Big endian assume CHAR_BIT == 8
//
template<typename T, typename... Ints>
T big_endian(const Ints& ... ints)
{
    int args[] { ints ... };
    T ret = 0;
    for (int i = 0; i < sizeof(args)/sizeof(args[0]); ++i) {
        ret <<= 8;
        ret |= (0xFF & args[i]);
    }
    return ret;
}

// Little endian assume CHAR_BIT = 8
//
template<typename T, typename... Ints>
T little_endian(const Ints& ... ints)
{
    int args[] { ints ... };
    T ret = 0;
    for (int i = 0; i < sizeof(args)/sizeof(args[0]); ++i) {
        ret |= ((0xFF & static_cast<T>(args[i])) << (i*8));
    }
    return ret;
}
