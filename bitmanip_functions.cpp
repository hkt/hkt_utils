
/* Sign extensions of a number.
 * Use the fact that the highest bit changes meaning from +2**p to -2**p
*/
int zero_or_sign_extend(int val, int sign_bit) {
    return val - 2*(val & sign_bit);
}
// where sign bit: 0 for unsigned and 1 << (bit_width - 1) for signed number

/* Different formulation:
*/
int zero_or_sign_extend(int val, int sign_bit) {
    return (val ^ sign_bit) - sign_bit;
}


template <typename T>
inline T reset_lowest_bit(T x) {
    return (T)(x & (x - 1));
}

template <typename T>
inline T isolate_lowest_bit(T x) {
    return (T)(x & (0 - x));
}







/// CountTrailing0Bits
	/// How to count the number of trailing zeroes in an unsigned 64 bit integer.
	/// Example:
	///     ...10101000 -> 3
	///     ...11111111 -> 0
	///     ...00000000 -> 64
	inline int CountTrailing0Bits(uint64_t x){
		#if defined(EA_COMPILER_MSVC) && defined(EA_PROCESSOR_X86_64)
			// This has been benchmarked as significantly faster than the generic code below.
			unsigned char isNonZero;
			unsigned long index;
			isNonZero = _BitScanForward64(&index, x);
			return isNonZero ? (int)index : 64;
		#elif defined(EA_COMPILER_GNUC) && !defined(EA_COMPILER_EDG)
			if(x)
				return __builtin_ctzll(x);
			return 64;
		#else
			if(x){
				int n = 1;
				if((x & 0xFFFFFFFF) == 0) { n += 32; x >>= 32; }
				if((x & 0x0000FFFF) == 0) { n += 16; x >>= 16; }
				if((x & 0x000000FF) == 0) { n +=  8; x >>=  8; }
				if((x & 0x0000000F) == 0) { n +=  4; x >>=  4; }
				if((x & 0x00000003) == 0) { n +=  2; x >>=  2; }
				return n - (int)(unsigned)(x & 1);
			}
			return 64;
		#endif
	}


	/// CountLeading0Bits
	/// How to count the number of leading zeroes in an unsigned integer.
	/// Example:
	///   ..00000000 -> 32
	///     00110111 ->  2
	///     11111111 ->  0
	///
	inline int CountLeading0Bits(uint32_t x){
		#if defined(EA_COMPILER_MSVC) && (defined(EA_PROCESSOR_X86) || defined(EA_PROCESSOR_X86_64))
			// This has been benchmarked as significantly faster than the generic code below.
			unsigned char isNonZero;
			unsigned long index;
			isNonZero = _BitScanReverse(&index, x);
			return isNonZero ? (31 - (int)index) : 32;
		#elif defined(EA_COMPILER_GNUC) && !defined(EA_COMPILER_EDG)
			if(x)
				return __builtin_clz(x);
			return 32;
		#else
			/// This implementation isn't highly compact, but would likely be 
			/// faster than a brute force solution.
			/// x86 function which is claimed to be faster:
			///    double ff = (double)(v|1);
			///    return ((*(1+(unsigned long *)&ff))>>20)-1023;
			if(x){
				int n = 0;
				if(x <= 0x0000FFFF) { n += 16; x <<= 16; }
				if(x <= 0x00FFFFFF) { n +=  8; x <<=  8; }
				if(x <= 0x0FFFFFFF) { n +=  4; x <<=  4; }
				if(x <= 0x3FFFFFFF) { n +=  2; x <<=  2; }
				if(x <= 0x7FFFFFFF) { n +=  1;           }
				return n;
			}
			return 32;
		#endif
	}

	/// CountLeading0Bits
	/// How to count the number of leading zeroes in an unsigned integer.
	/// Example:
	///   ..00000000 -> 64
	///   ..00110111 ->  2
	///   ..11111111 ->  0
	///
	inline int CountLeading0Bits(uint64_t x){
		#if defined(EA_COMPILER_MSVC) && defined(EA_PROCESSOR_X86_64)
			// This has been benchmarked as significantly faster than the generic code below.
			unsigned char isNonZero;
			unsigned long index;
			isNonZero = _BitScanReverse64(&index, x);
			return isNonZero ? (63 - (int)index) : 64;
		#elif defined(EA_COMPILER_GNUC) && !defined(EA_COMPILER_EDG)
			if(x)
				return __builtin_clzll(x);
			return 64;
		#else
			if(x){ // We use a slightly different algorithm than the 32 bit version above because this version avoids slow large 64 bit constants, especially on RISC processors.
				int n = 0;
				if(x & UINT64_C(0xFFFFFFFF00000000)) { n += 32; x >>= 32; }
				if(x & 0xFFFF0000)                   { n += 16; x >>= 16; }
				if(x & 0xFFFFFF00)                   { n +=  8; x >>=  8; }
				if(x & 0xFFFFFFF0)                   { n +=  4; x >>=  4; }
				if(x & 0xFFFFFFFC)                   { n +=  2; x >>=  2; }
				if(x & 0xFFFFFFFE)                   { n +=  1;           }
				return 63 - n;
			}
			return 64;
		#endif
	}



	/// CountBits
	/// How to count the number of bits in an unsigned integer.
	/// There are multiple variations of this function available, 
	/// each tuned to the expected bit counts and locations. 
	/// The version presented here has a large number of logical 
	/// operations but has no looping or branching.
	/// This implementation is taken from the AMD x86 optimization guide.
	/// Example:
	///     11001010 -> 4
	inline int CountBits(uint32_t x) // Branchless version
	{ 
	#if defined(EASTDC_SSE_POPCNT)
		return _mm_popcnt_u32(x);
	#else
		x = x - ((x >> 1) & 0x55555555);
		x = (x & 0x33333333) + ((x >> 2) & 0x33333333);
		x = (x + (x >> 4)) & 0x0F0F0F0F;
		return (int)((x * 0x01010101) >> 24);
	#endif
	}

	inline int CountBits64(uint64_t x) // Branchless version
	{ 
	#if defined(EASTDC_SSE_POPCNT) && defined(EA_PROCESSOR_X86_64)
		return (int)_mm_popcnt_u64(x);
	#else
		#if (EA_PLATFORM_WORD_SIZE < 8)
			return CountBits((uint32_t)(x >> 32)) + CountBits((uint32_t)x);
		#else
			x = x - ((x >> 1) & UINT64_C(0x5555555555555555));
			x = (x & UINT64_C(0x3333333333333333)) + ((x >> 2) & UINT64_C(0x3333333333333333));
			x = (x + (x >> 4)) & UINT64_C(0x0F0F0F0F0F0F0F0F);
			return (int)((x * UINT64_C(0x0101010101010101)) >> 56);
		#endif
	#endif
	}

	/// RotateLeft
	/// How to rotate an integer left.
	/// Bit rotations can often be accomplished with inline assembly 
	/// that uses the processor's native bit rotation capabilities.
	/// Example:
	///     11110000 -> left 2 -> 11000011
	inline uint32_t RotateLeft(uint32_t x, uint32_t n){
		return (uint32_t)((x << n) | (x >> (32 - n)));
	}

	inline uint64_t RotateLeft(uint64_t x, uint64_t n){
		return (uint64_t)((x << n) | (x >> (64 - n)));
	}

	inline uint32_t RotateRight(uint32_t x, uint32_t n){
		return (uint32_t)((x >> n) | (x << (32 - n)));
	}

	inline uint64_t RotateRight(uint64_t x, uint64_t n){
		return (uint64_t)((x >> n) | (x << (64 - n)));
	}

	/// ReverseBits
	/// How to reverse the bits in an integer.
	/// There are other mechansims for accomplishing this. 
	/// The version presented here has no branches, looping, nor table lookups.
	/// Table lookups are the fastest when done on large amounts of data, but slower
	/// when just done once due to the initial lookup cost.
	/// Some ARM results: http://corner.squareup.com/2013/07/reversing-bits-on-arm.html
	/// http://stackoverflow.com/questions/746171/best-algorithm-for-bit-reversal-from-msb-lsb-to-lsb-msb-in-c
	/// Example:
	///    11100001 -> 10000111
	inline uint32_t ReverseBits(uint32_t x){
		x = ((x & 0x55555555) << 1) | ((x >> 1) & 0x55555555);
		x = ((x & 0x33333333) << 2) | ((x >> 2) & 0x33333333);
		x = ((x & 0x0F0F0F0F) << 4) | ((x >> 4) & 0x0F0F0F0F);
		x = (x << 24) | ((x & 0xFF00) << 8) | ((x >> 8) & 0xFF00) | (x >> 24);
		return x;
	}

	// This is surely not the fastest way to do this, and is here for completeness only.
	// To do: Make a better version of this.
	inline uint64_t ReverseBits(uint64_t x){
		uint32_t x1 = (uint32_t)(x & 0xffffffff);
		uint32_t x2 = (uint32_t)(x >> 32);
		return ((uint64_t)ReverseBits(x1) << 32) | ReverseBits(x2);
	}



