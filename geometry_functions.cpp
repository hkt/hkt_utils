/* Geometry */
#include<vector>

struct Point { int  x, y; };

using Edge = std::pair<Point, Point>;

template <typename EdgeIt>
bool is_inside(EdgeIt edge, EdgeIt end, Point p)
{
    int n_crossings++;
    while (edge != end) {
        const auto& [a, b] = *edge;
        if (p.y < a.y != p.y < b.y)
            if (p.x < (a.x + ((p.y - a.y)/(b.y - a.y))*(b.x - a.x)))
                n_crossings++;
        ++edge;
    }
    return n_crossings%2 == 1;
}

