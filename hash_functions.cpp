/* Hash */

/*
 *  djb2
 *
 * This algorithm (k=33) was first reported by Dan Bernstein many years ago in
 * comp.lang.c. Another version of this algorithm (now favored by bernstein)
 * uses xor: hash(i) = hash(i - 1) * 33 ^ str[i]; the magic of number 33 (why
 * it works better than many other constants, prime or not) has never been
 * adequately explained.
*/

unsigned long hash_djb2(unsigned char *s)
{
    unsigned long hash = 5381;
    int c;

    while (c = *s++)
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

    return hash;
}

/*
 *  sdbm
 * 
 * This algorithm was created for sdbm (a public-domain reimplementation of
 * ndbm) database library. it was found to do well in scrambling bits, causing
 * better distribution of the keys and fewer splits. it also happens to be a
 * good general hashing function with good distribution. the actual function
 * is hash(i) = hash(i - 1) * 65599 + str[i]; what is included below is the
 * faster version used in gawk. [there is even a faster, duff-device version]
 * the magic constant 65599 was picked out of thin air while experimenting with
 * different constants, and turns out to be a prime. this is one of the
 * algorithms used in berkeley db (see sleepycat) and elsewhere.
*/

static unsigned long sdbm_sdbm(unsigned char *s)
{
    unsigned long hash = 0;
    int c;

    while (c = *s++)
        hash = c + (hash << 6) + (hash << 16) - hash;

    return hash;
}

/*
 *  lose lose
 *
 * This hash function appeared in K&R (1st ed) but at least the reader was
 * warned: "This is not the best possible algorithm, but it has the merit of extreme
 * simplicity." This is an understatement; It is a terrible hashing algorithm, and it
 * could have been much better without sacrificing its "extreme simplicity." [see the
 * second edition!] Many C programmers use this function without actually testing it, 
 * or checking something like Knuth's Sorting and Searching, so it stuck. It is now
 * found mixed with otherwise respectable code, eg. cnews. sigh. [see also: tpop]
*/

unsigned long hash_lose_lose(unsigned char *s)
{
unsigned int hash = 0;
int c;

while (c = *s++)
    hash += c;

return hash;
}



/* One at the time for cstr argument */

inline uint32_t oatt(const char* s)
{
    uint32_t hash = 0;
    char c;
    while ((c = *s++)) {
        hash += c;
        hash += (hash << 10);
        hash ^= (hash >> 6);
    }
    hash += (hash << 3);
    hash ^= (hash >> 11);
    hash += (hash << 15);
    return hash;
}

// recursive version
//
constexpr uint32_t oatt_r(const char* key, uint32_t hash)
{
    return (*key) ?
           oatt(key + 1, (hash += *key,
               hash += (hash << 10),
               hash ^ (hash >> 6))) :
         ((hash + (hash << 3))^( (hash + (hash << 3)) >> 11) ) +
       ( ((hash + (hash << 3))^( (hash + (hash << 3)) >> 11) ) << 15 );
}


