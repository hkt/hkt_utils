/* Meta */

// Iterator implementation
//
class iterator_t {
 public:
  using iterator_category = std::forward_iterator_tag;
  using value_type = T;
  using difference_type = std::ptrdiff_t;
  using pointer = T*;
  using reference = T&;
};

// Size of an array 
// array parameter has no name, because we care only about
//
template<typename T, std::size_t N>                 
constexpr std::size_t arraySize(T (&)[N]) noexcept  
{                                                   
  return N;                                         
}                                                   


// Display the type deduced by the compiler (AS ERROR!)
// 
template<class T> class that_type;
template<class T> void name_that_type(T& param)
{
    that_type<T> tType;
    that_type<decltype(param)> paramType;
}

// Variadic template - adder example
//
template<typename T>
T adder(T v) {
  return v;
}

template<typename... Args, typename T>
T adder(Args... args, T last) {
  return last + adder(args...);
}

// Generate array of powers of 10 // maybe constexpr iota from c++20 ?
// Usage: auto dec_powers = pow10i<5>::array();
//
template <int E>
class pow10i
{
    public:
        static constexpr auto array() {
            return pow10i_array(std::make_index_sequence<E + 1>{});
        }
    private:
        static constexpr long long value(int e) {
            return e == 0 ? 1 : 10*value(e - 1);
        }
        template<size_t ... i>
            static constexpr auto pow10i_array(std::index_sequence<i...>) {
                return std::array<long long, sizeof...(i)>{{value(i)...}};
            }
};

